//
//  DetailViewController.h
//  MasterDetailDemo
//
//  Created by James Cash on 19-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WarCriminal.h"

@class DetailViewController;

@protocol DetailViewDelegate

- (void)viewController:(DetailViewController*)vc changedStateOf:(WarCriminal*)criminal;

@end

@interface DetailViewController : UIViewController

@property (nonatomic,strong) WarCriminal *item;

@property (nonatomic,weak) id<DetailViewDelegate> delegate;

@end
