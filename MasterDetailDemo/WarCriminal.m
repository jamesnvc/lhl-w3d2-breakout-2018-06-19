//
//  WarCriminal.m
//  MasterDetailDemo
//
//  Created by James Cash on 19-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "WarCriminal.h"

@implementation WarCriminal

- (instancetype)initWithName:(NSString *)name reason:(NSString *)reason atLarge:(BOOL)atLarge
{
    self = [super init];
    if (self) {
        _name = name;
        _reason = reason;
        _atLarge = atLarge;
    }
    return self;
}

@end
