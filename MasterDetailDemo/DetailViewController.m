//
//  DetailViewController.m
//  MasterDetailDemo
//
//  Created by James Cash on 19-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *reasonLabel;
@property (strong, nonatomic) IBOutlet UILabel *atLargeLabel;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.nameLabel.text = self.item.name;
    self.reasonLabel.text = self.item.reason;
    // ternary statement -- like if-then-else
    self.atLargeLabel.text = self.item.atLarge ? @"AT LARGE" : @"UNDER CONTROL";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// this is a bad name but I'm tired
- (IBAction)toggleStatus:(id)sender {
    self.item.name = [self.item.name stringByAppendingString:@"💩"];
    [self.delegate viewController:self changedStateOf:self.item];
}

@end
