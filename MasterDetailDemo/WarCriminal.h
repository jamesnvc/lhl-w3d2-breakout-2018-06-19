//
//  WarCriminal.h
//  MasterDetailDemo
//
//  Created by James Cash on 19-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WarCriminal : NSObject

@property (nonatomic,strong) NSString* name;
@property (nonatomic,strong) NSString* reason;
@property (nonatomic,assign) BOOL atLarge;

- (instancetype)initWithName:(NSString*)name reason:(NSString*)reason atLarge:(BOOL)atLarge;

@end
