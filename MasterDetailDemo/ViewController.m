//
//  ViewController.m
//  MasterDetailDemo
//
//  Created by James Cash on 19-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "WarCriminal.h"
#import "CriminalCell.h"
#import "DetailViewController.h"

@interface ViewController () <UITableViewDataSource,UITableViewDelegate,DetailViewDelegate>

@property (nonatomic,strong) NSArray* data;
@property (nonatomic,strong) UITableView *tableView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.data = @[[[WarCriminal alloc] initWithName:@"Henry Kissinger" reason:@"Bombing of Cambodia" atLarge:YES],
                  [[WarCriminal alloc] initWithName:@"Milosevic" reason:@"Ethinic cleansing" atLarge:NO],
                  [[WarCriminal alloc] initWithName:@"Dick Cheney" reason:@"Iraq War" atLarge:YES]];

    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero
                                                  style:UITableViewStylePlain];
    [self.view addSubview:self.tableView];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.tableView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor].active = YES;
    [self.tableView.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
    [self.tableView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
   [self.tableView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor].active = YES;

    self.tableView.dataSource = self;
    self.tableView.delegate = self;

    // this is the equivalent of setting the reuse identifier in Storyboard
    [self.tableView registerClass:[CriminalCell class]
           forCellReuseIdentifier:@"criminalCell"];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowDetailSegue"]) {
        DetailViewController *dvc = segue.destinationViewController;
        WarCriminal *detailItem = sender;
        // in this case, we made the sender the data object itself
        // some other scenarios are:
        //  - sender is the cell, in which case you can ask the tableView for the indexPAth of that cell & use that to get the data objects
        //  - sender is your custom cell class that you give a property to store the data object & get it from that
        //  - sender is something completely unreleated & you can use the method on tableview indexPathForSelectedRow & use that to get the selected items
        dvc.item = detailItem;
        dvc.delegate = self;
        // we can't just set the labels of the second view controller to be what we want, because at this point the view hasn't been created yet (because it's not on screen yet)
        // in any case though, it's generally not great to directly set the views of some other view controller
        
    }
}

#pragma mark - DetailViewDelegate

- (void)viewController:(DetailViewController *)vc changedStateOf:(WarCriminal *)criminal
{
    // in this case, we don't need to update the contents of our data array, because the criminal object here has just been directly modified anyway
    // but in general, this would be where we'd get some new object and have to do something with it (stick in the array, add it to some object, ???)
    NSUInteger idx = [self.data indexOfObject:criminal];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:0];
    [[self.tableView cellForRowAtIndexPath:indexPath] setBackgroundColor:UIColor.redColor];
    // important to keep in mind that just because the data source changes, the tableview *does not* know about that
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade]; // specfically reloading one row
    // could also use, in general
//    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data.count; // tableview just sees a number, not that the number is the length of an array
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CriminalCell* cell = [tableView dequeueReusableCellWithIdentifier:@"criminalCell"];

    WarCriminal *criminal = self.data[indexPath.row]; // we're getting the information to display from an array, but tableview just knows that we've given it a cell & put some information on it
    // probably want to make a configCell: method on our class instead of directly setting outlets
    cell.textLabel.text = criminal.name;

    return cell;
}

#pragma mark UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // this would be like what would happen if we had made the segue by dragging from the cell in the storyboard
//    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
//    [self performSegueWithIdentifier:@"ShowDetailSegue" sender:cell];

    // or we could just pass the data object itself as the sender
    WarCriminal *criminal = self.data[indexPath.row];
    [self performSegueWithIdentifier:@"ShowDetailSegue" sender:criminal];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

@end
