//
//  CriminalCell.m
//  MasterDetailDemo
//
//  Created by James Cash on 19-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "CriminalCell.h"

@implementation CriminalCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
